﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace grawzycie
{
    class Swiat
    {
        public Swiat(int rozmiar)
        {
            rozmiarPlanszy = rozmiar;
            plansza = new int[rozmiarPlanszy, rozmiarPlanszy];
            planszaKopia = new int[rozmiarPlanszy, rozmiarPlanszy];
            for (int i = 0; i < rozmiarPlanszy; i++)
            {
                for (int j = 0; j < rozmiarPlanszy; j++)
                {
                    plansza[i, j] = 0;
                }
            }
        }

        public void WypelnijPlansze()
        {
            Random x = new Random();
            for (int i = 0; i < rozmiarPlanszy; i++)
            {
                for (int j = 0; j < rozmiarPlanszy; j++)
                {
                    plansza[i, j] = x.Next(0, 2);
                }
            }
        }

        public void PokazTablice()
        {
            for (int i = 0; i < rozmiarPlanszy; i++)
            {
                for (int j = 0; j < rozmiarPlanszy; j++)
                {
                    Console.Write(plansza[i, j] + " ");
                }
                Console.WriteLine();
            }
        }

        private bool CzyIstnieje(int i, int j)
        {
            if (i >= 0 && i < rozmiarPlanszy && j >= 0 && j < rozmiarPlanszy)
                return true;
            else
                return false;

        }

        public void KolejnyCykl()
        {
            SkopiujPlansze();

            for (int i = 0; i < rozmiarPlanszy; i++)
            {
                for (int j = 0; j < rozmiarPlanszy; j++)
                {
                    int licznik = 0;

                    if (CzyIstnieje(i - 1, j - 1))
                        if (planszaKopia[i - 1, j - 1] == 1)
                            licznik++;

                    if (CzyIstnieje(i - 1, j))
                        if (planszaKopia[i - 1, j] == 1)
                        licznik++;

                    if (CzyIstnieje(i - 1, j + 1))
                        if (planszaKopia[i - 1, j + 1] == 1)
                        licznik++;

                    if (CzyIstnieje(i + 1, j - 1))
                        if (planszaKopia[i + 1, j - 1] == 1)
                        licznik++;

                    if (CzyIstnieje(i + 1, j))
                        if (planszaKopia[i + 1, j] == 1)
                        licznik++;

                    if (CzyIstnieje(i + 1, j + 1))
                        if (planszaKopia[i + 1, j + 1] == 1)
                        licznik++;

                    if (CzyIstnieje(i, j + 1))
                        if (planszaKopia[i, j + 1] == 1)
                        licznik++;

                    if (CzyIstnieje(i, j - 1))
                        if (planszaKopia[i, j - 1] == 1)
                        licznik++;



                    if (licznik < 2 && planszaKopia[i, j] == 1)
                        plansza[i, j] = 0;
                    if ((licznik == 2 || licznik == 3) && planszaKopia[i, j] == 1)
                        plansza[i, j] = 1;
                    if (licznik > 3 && planszaKopia[i, j] == 1)
                        plansza[i, j] = 0;
                    if (planszaKopia[i, j] == 0 && licznik == 3)
                        plansza[i, j] = 1;

                }
            }
        }

        private void SkopiujPlansze()
        {
            for (int i = 0; i < rozmiarPlanszy; i++)
            {
                for (int j = 0; j < rozmiarPlanszy; j++)
                {
                    planszaKopia[i, j] = plansza[i, j];
                }
            }
        }

        public void StworzPlansze()
        {
            for (int i = 0; i < rozmiarPlanszy; i++)
            {
                string linijka = Console.ReadLine();
                string[] wartosci = linijka.Split(' ');
                for (int j = 0; j < rozmiarPlanszy; j++)
                {
                    plansza[i, j] = int.Parse(wartosci[j]);
                }
            }
        }

        int[,] plansza;
        int[,] planszaKopia;
        int rozmiarPlanszy;

    }
}
