﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace grawzycie
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Console.WriteLine("1 - Stworz plansze");
            Console.WriteLine("2 - Wygeneruj losowa plansze");
            Console.Write("Wybor: ");
            string wybor = Console.ReadLine();
            int rozmiar;
            Swiat zycie;


            switch (wybor)
            {
                case "1":
                    Console.Write("Podaj rozmiar planszy: ");
                    rozmiar = int.Parse(Console.ReadLine());
                    zycie = new Swiat(rozmiar);
                    zycie.StworzPlansze();
                    break;
                case "2":
                    Console.Write("Podaj rozmiar planszy: ");
                    rozmiar = int.Parse(Console.ReadLine());
                    zycie = new Swiat(rozmiar);
                    zycie.WypelnijPlansze();
                    break;
                default:
                    Console.WriteLine("Podano zla opcje!");
                    return;
                    
            }

            Console.WriteLine();
            zycie.PokazTablice();

            while(true)
            {
                if (Console.ReadKey(true).Key == ConsoleKey.Enter)
                {
                    Console.Clear();
                    zycie.PokazTablice();
                    zycie.KolejnyCykl();
                    Console.WriteLine();
                    zycie.PokazTablice();
                }
                else
                    break;
            }

        }
    }
}
